module.exports={
    bitcoind:{
         rpc_host:'35.170.128.185',
        //rpc_host:'127.0.0.1',
        rpc_port:9005,
        rpc_user:"rpcUser",
        rpc_pass:"rpcPass",
        reserved_mature_block: 144,/**144 block ~ 1 day. Only sync to (lasted block - reserved_mature_block) to avoid sync fork chain */
        req_period:30 /*time between 2 rpc call in ms */
    },
    mongodb:{
        host:'127.0.0.1',
        //host:'35.170.128.185',
        port:27017,
        dtb_name:"btc_info",
        addr_info_collection:{
            name: "address_info",
            n_max_element: 50000 /** each array maximux 50K element*/
        },
        block_info_collection:{
            name: "block_info"
        },
        tx_info_collection:{
            name: "transaction_info"
        }
    }
}