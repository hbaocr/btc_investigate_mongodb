#!/bin/bash
#https://docs.mongodb.com/manual/tutorial/rotate-log-files/#Logging-Rotatingthelogfiles
echo "Step 1:>>>>>>>>>>> StartconfigServer fork>>>>>>>>>>"
replSetID=csrs0
mountPath=/home/ubuntu/mongoServConfig
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
port=27018
bindIp=0.0.0.0
mongod --configsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend 
sleep 1

echo "Step 2:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=0
port=27019
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 3:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=1
port=27020
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 4:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=2
port=27021
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 5:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=3
port=27022
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 6:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=4
port=27023
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 7:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=5
port=27024
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 8:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
shardNum=6
port=27025
replSetID=rs$shardNum
mountPath=/home/ubuntu/shard$shardNum
dbpathName=$mountPath/data
logFile=$mountPath/log.txt
bindIp=0.0.0.0
echo "Shard${shardNum} is mounted at path : ${mountPath}"
echo "network: ${replSetID}/${bindIp}:${port}"
mongod  --shardsvr --replSet $replSetID  --bind_ip $bindIp  --port $port  --dbpath $dbpathName --fork --logpath $logFile --logRotate reopen --logappend
sleep 1

echo "Step 9: >>>>>>>>>MongosRouter>>>>>>>>>>>>"
mountPath=/home/ubuntu/mongosRouter
#logFile=$mountPath/log.txt
#disbale log
logFile=/dev/null
#the config server para from step 1
cfgServerReplica=csrs0
serverCfgPort=27018
serverCfgIP=127.0.0.1
#mogos router interface
bindIP=0.0.0.0
routerPort=27017
echo "Mongos Router is mounted at path : ${mountPath}"
echo "network: ${bindIp}:${routerPort}"
mongos --configdb "${cfgServerReplica}/${serverCfgIP}:${serverCfgPort}" --port $routerPort --bind_ip $bindIP --fork --logpath $logFile --logRotate reopen --logappend
sleep 1
echo "Step 10: >>>>>>>>>Check status>>>>>>>>>>>>"
netstat -plntu