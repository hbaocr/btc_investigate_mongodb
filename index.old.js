/**
 * TX_info collection Schema :
 * 
 * _id = txid
 * // these 2 fields are updated through background task check
 * dark_height = min {dark_derived_from.txid.dark_height}?min {dark_derived_from.txid.dark_height}+1 : 0
 * dark_derived_from =[{txid ,vout},{txid,vout}, .... ] 
 * 
 * // these are updated from bitcoind node. vout =-1 (coinbase txid)
 * light_derived_froms =[{txid ,vout},{txid,vout}, ....] // full from Txid
 * to_addresses   = [{addresses: [ad1, ad2,...],tx_type,amout_delta} , ...]
 * date =
 * block=
 */

/****
 * Address_info collection schema
 * 
 * _id : address
 * txid_to_this =[{txid, block_num,time,amount_sat} , .... ]
 * 
 */


const sys_config = require('./config/config');
const mongodb_client = require('./driver/mongodb_client');
const rpc_btc = require('./driver/btc_rpc');
//const parse_block_utils = require('./parse_btc_block');
const mongodb_btc = require('./mongodb_btc');

const dtb_name = sys_config.mongodb.dtb_name;
const rpc_host = `http://${sys_config.bitcoind.rpc_host}:${sys_config.bitcoind.rpc_port}`;
const block_collection = sys_config.mongodb.block_info_collection.name;
const txid_collection = sys_config.mongodb.tx_info_collection.name;
const adrr_collection = sys_config.mongodb.addr_info_collection.name;

//change to synchronus way to avoid the error "MongoNetworkError: write EPIPE"

async function main_app() {

    let start_block_height = 1;

    //=====================get last saved block_info from database================================== 
    let start = await mongodb_client.query_data(dtb_name, block_collection, {}, {}, 1, { _id: -1 });
   
    if (start.length == 0)//no dtb save before
    {
        //message:"The genesis block 0 : coinbase is not considered an ordinary transaction and cannot be retrieved".
        console.log('restart at block ', start_block_height);
    } else {
        start_block_height = start[0].height; //update curent
        console.log('restart at block ', start_block_height);
    }
    
    //start_block_height = 164528;

    let cmd = rpc_btc.build_cmd_getblockhash(start_block_height);
    let res = await rpc_btc.do_http_rpc(rpc_host, cmd);
    await rpc_btc.delay_promise(sys_config.bitcoind.req_period);
    let block_hash = res.data.result;
    let nextblockhash = undefined;
    do {
        await rpc_btc.delay_promise(sys_config.bitcoind.req_period);
        cmd = rpc_btc.build_cmd_getblock(block_hash);
        res = await rpc_btc.do_http_rpc(rpc_host, cmd);
        nextblockhash = res.data.result.nextblockhash;
        if (nextblockhash == undefined) {
            console.log('reach the current block.Wait for new block comming.....')
            await rpc_btc.delay_promise(10000);//10 sec to check if new block is comming
            continue;
        }

        //=====================Save block info into database=============================================== 
        console.log('**************************************block heigh = ' + res.data.result.height + '   blockhash = ' + block_hash);
        let json_block = rpc_btc.block_obj_to_json(res.data.result);

        try {
            let r = await mongodb_client.insert_one_data(dtb_name, block_collection, json_block);
            console.log('save block_info to mongodb ');
        } catch (e) {
            if (e.code == 11000) {
                console.log('********duplicate block data( already saved)');
            } else {
                console.log('---->try again');
                console.log('---->' + e);
                continue;
            }
        }

        //=======================Get all data in block from the full node============================================== 
        let json_batch = res.data.result.tx.map((e) => {
            return rpc_btc.build_cmd_getrawtransaction(e);
        })

        let height = res.data.result.height;


        let rpc_batch_res;
        while (1) {
            try {
                rpc_batch_res = await rpc_btc.do_http_rpc(rpc_host, json_batch);
                rpc_batch_res.height = height; //add more blockheight field
                break;
            } catch (e) {
                console.log('batch request rpc failed --> try again');
                await rpc_btc.delay_promise(1000);//1 sec to retry
            }
        }

        //========================Save tx_info into database=============================================== 
        let tx_info = mongodb_btc.build_tx_schema_mongodb_ops(rpc_batch_res);
        //to AVOID OVERLOAD for mongodb--> chunk to smaller size
        let chunk_sz = 500;
        let n_tx = tx_info.length;
        let n_chunk = Math.ceil(n_tx / chunk_sz);
        let tt = 0;
        for (let i = 0; i < n_chunk;) {
            await rpc_btc.delay_promise(50);//50 msec
            let end_p = chunk_sz * (i + 1);
            if (end_p > n_tx) {
                end_p = n_tx;
            }
            let smaller_tx_chunk = tx_info.slice(chunk_sz * i, end_p);
            try {
                let r = await mongodb_client.insert_many_data(dtb_name, txid_collection, smaller_tx_chunk);
                tt = tt + smaller_tx_chunk.length;
                i++;//move next
                console.log('In block ' + height + ', save tx_info to mongodb:  ' + (tt) + ' / ' + n_tx);
            } catch (e) {
                if (e.code == 11000) {
                    tt = tt + smaller_tx_chunk.length;
                    i++;//move next
                    console.log('In block ' + height + ', (duplicate)saved tx_info before to mongodb:  ' + (tt) + ' / ' + n_tx);
                } else {
                    console.log('---->' + e);
                }
            }
            
        }
        //========================Save addr_info into database=============================================== 
        let addr_info = mongodb_btc.build_addr_schema_mongodb_ops(rpc_batch_res);
        for (let i = 0; i < addr_info.length; ) {
            await rpc_btc.delay_promise(10);//10 msec to check if new block is comming
            let v = addr_info[i];
            try{
                let r = await mongodb_client.update_info_schema(dtb_name, adrr_collection, v.filter, v.update_query);
                console.log('In block ' + height + ' ,save addr_info to mongodb: ' + (i + 1) + ' / ' + addr_info.length);
                i++;//move next
            }catch(e){
                if(e.code==11000){
                    console.log('In block ' + height + ' ,(duplicate)saved addr_info before to mongodb: ' + (i + 1) + ' / ' + addr_info.length);
                    i++;//move next
                }else{
                    console.log(e);
                }
            }
        }
        block_hash = nextblockhash;
    } while (1 == 1)
}

main_app();
