const axios = require('axios');
const sys_config = require('../config/config');
const newLine = "\r\n";
const rpc_user = sys_config.bitcoind.rpc_user;
const rpc_pass = sys_config.bitcoind.rpc_pass;
let config = {
    auth: {
        username: rpc_user,
        password: rpc_pass
    },
    headers: {
        'Content-Type': 'text/plain'
    }
}

function set_rpc_config(user, pass) {
    config.auth.username = user;
    config.auth.password = pass;
}
function chunk(array, size = 300) {
    const chunked_arr = [];
    let index = 0;
    while (index < array.length) {
        chunked_arr.push(array.slice(index, size + index));
        index += size;
    }
    return chunked_arr;
}

function build_cmd_getblockhash(blk_num) {
    return { "jsonrpc": "2.0", "id": Date.now().toString(), "method": "getblockhash", "params": [blk_num] };
}
function build_cmd_getblockcount() {
    return { "jsonrpc": "2.0", "id": Date.now().toString(), "method": "getblockcount"};
}
function build_cmd_getblock(block_hash) {
    return { "jsonrpc": "2.0", "id": Date.now().toString(), "method": "getblock", "params": [block_hash] };
}
function build_cmd_getrawtransaction(txid) {
    return { "jsonrpc": "2.0", "id": Date.now().toString(), "method": "getrawtransaction", "params": [txid, 1] };
}
function do_http_rpc(node_link, rpc_json) {
    let rpc_cmd = JSON.stringify(rpc_json);
    return axios.post(node_link, rpc_cmd, config)
}
function block_obj_to_json(getblockresult) {
    return {
        _id: getblockresult.height,
        height: getblockresult.height,
        txIds: getblockresult.tx,
        nTxs: getblockresult.tx.length,
        perviousblockhash: getblockresult.previousblockhash,
        blockminingtime: getblockresult.time,
        blockmediantime: getblockresult.mediantime,
        merkletreeroot: getblockresult.merkleroot,
        blockbits: getblockresult.bits,
        blocknonce: getblockresult.nonce,
        blockversion: getblockresult.version,
        blocksize: getblockresult.size,
        blockstripedsize: getblockresult.strippedsize,
        blockweight: getblockresult.weight,
        blockchainwork: getblockresult.chainwork
    }
}
function rawtx_obj_to_json(txatribute) {
    let array_vin = txatribute.vin.map((vin) => {
        if (vin.coinbase == undefined) {
            return {
                txid: vin.txid,
                vout: vin.vout,
                sequence: vin.sequence,
                scriptSig: vin.scriptSig.asm,
                txinwitness: vin.txinwitness,
            }
        } else {
            //coinbase TX
            return {
                txid: 'coinbase_' + vin.coinbase + '_' + vin.sequence,
                sequence: vin.sequence
            }

        }
    });

    let array_vout = txatribute.vout.map((v) => {
        return {
            address: v.scriptPubKey.addresses,
            value: v.value,
            idx: v.n,
            scriptPubKey: v.scriptPubKey.asm,
            type: v.scriptPubKey.type,
            reqSigs: v.scriptPubKey.reqSigs
        }
    })
    return {
        _id: txatribute.txid,
        txid: txatribute.txid,
        idx: txatribute.idx,
        height: txatribute.height,
        hash: txatribute.hash,
        version: txatribute.version,
        size: txatribute.size,
        vsize: txatribute.vsize,
        locktime: txatribute.locktime,
        time: txatribute.time,
        vins: array_vin,
        vouts: array_vout
    }
}
function delay_promise(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
module.exports.delay_promise = delay_promise;
module.exports.build_cmd_getblockhash  = build_cmd_getblockhash;
module.exports.build_cmd_getblockcount = build_cmd_getblockcount;
module.exports.build_cmd_getblock = build_cmd_getblock;
module.exports.build_cmd_getrawtransaction = build_cmd_getrawtransaction;
module.exports.do_http_rpc = do_http_rpc;
module.exports.chunk_array = chunk;
module.exports.set_rpc_config = set_rpc_config;
module.exports.block_obj_to_json = block_obj_to_json;
module.exports.rawtx_obj_to_json = rawtx_obj_to_json;