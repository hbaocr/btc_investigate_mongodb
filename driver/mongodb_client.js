//----------------Example------------------------------------------------ 
//let data=[{ "_id" : 10, "title":"MongoDB Overview"},
// { "_id" : 20, "title":"NoSQL Overview"},
// { "_id" : 30, "title":"Tutorials Point Overview"}];
//mongodb_client.insert_one_data(dtb_name,'block',{_id:1,info:2});
//mongodb_client.insert_many_data(dtb_name,'block',data);
// let filter={fields:{_id: 0}}
// mongodb_client.query_data(dtb_name,'block',{},filter).then(console.log);


// let aa=[
//     { item: "journal", instock: [ { warehouse: "A", qty: 5 }, { warehouse: "C", qty: 15 } ] },
//     { item: "notebook", instock: [ { warehouse: "C", qty: 5 } ] },
//     { item: "paper", instock: [ { warehouse: "A", qty: 60 }, { warehouse: "B", qty: 15 } ] },
//     { item: "planner", instock: [ { warehouse: "A", qty: 40 }, { warehouse: "B", qty: 5 } ] },
//     { item: "postcard", instock: [ { warehouse: "B", qty: 15 }, { warehouse: "C", qty: 35 } ]}];

// //mongodb_client.insert_many_data(dtb_name,'hello',aa).then(console.log);
// //let query={ instock: { warehouse: "A", qty: 5 } };
// let query={ 'instock.warehouse': 'A'}
// mongodb_client.query_data(dtb_name,'hello',query).then((res)=>{console.log(JSON.stringify(res))});


const MongoClient = require('mongodb').MongoClient;
const config = require('../config/config');

let mogodb_url = `mongodb://${config.mongodb.host}:${config.mongodb.port}`;



function update_info_schema(dtb_name, collection_name, filter, json_update_ops,options = { upsert: true }) {
    return new Promise((resolve, reject) => {
        let url = mogodb_url;
        let dtb;
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err);
            } else {
                dtb = db;
                let dbo = db.db(dtb_name);
                /*
                    upsert. When true, updateMany() either:
                    Creates a new document if no documents match the filter. For more details see upsert behavior.
                    Updates documents that match the filter.
                    To avoid multiple upserts, ensure that the filter fields are uniquely indexed.
                 */
                //let options = { upsert: true };
                //mongodb auto create the dtb and collection  if they don't exist.
                dbo.collection(collection_name).updateOne(filter, json_update_ops, options, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                      //  console.log('Document update ok');
                        dtb.close();
                    }
                })

            }
        });
    })
}

function create_dtb(dtb_name) {
    return new Promise((resolve, reject) => {
        let url = mogodb_url + '/' + dtb_name;
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err);
            } else {
                console.log(dtb_name + " Database is created!Then close connection");
                db.close();
                resolve();
            }
        });
    })
}
function create_collection(dtb_name, collection_name) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(mogodb_url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err)
            } else {
                let dbo = db.db(dtb_name);
                dbo.createCollection(collection_name, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        console.log("Collection created!");
                        db.close();
                        resolve(res);
                    }
                });
            }

        });
    })
}
function insert_one_data(dtb_name, collection_name, json_document) {
    return new Promise((resolve, reject) => {
        let url = mogodb_url;
        let dtb;
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err);
            } else {
                dtb = db;
                let dbo = db.db(dtb_name);
                //mongodb auto create the dtb and collection  if they don't exist.
                dbo.collection(collection_name).insertOne(json_document, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                        //console.log('document insert ok');
                        dtb.close();
                    }
                })

            }
        });
    })
}
function insert_many_data(dtb_name, collection_name, json_array_document) {
    return new Promise((resolve, reject) => {
        let url = mogodb_url;
        let dtb;
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err);
            } else {
                dtb = db;
                let dbo = db.db(dtb_name);
                //mongodb auto create the dtb and collection  if they don't exist.
                dbo.collection(collection_name).insertMany(json_array_document, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                        console.log('number of inserted documents:  ', res.insertedCount);
                        dtb.close();
                    }
                })

            }
        });
    })
}
function query_data(dtb_name, collection_name, json_queries = {}, projection = {}, limit_record = -1, sort_opt = { _id: 1 }) {
    return new Promise((resolve, reject) => {
        let url = mogodb_url;
        let dtb;
        MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
            if (err) {
                reject(err);
            } else {
                dtb = db;
                let dbo = db.db(dtb_name);
                //mongodb auto create the dtb and collection  if they don't exist.
                // projection={fields:{_id: 0}} ===> to turn of displaying of field _Id
                if (limit_record <= 0) { //list all
                    dbo.collection(collection_name).find(json_queries, projection).toArray(function (err, result) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                            console.log('found documents:  ', result.length);
                            dtb.close();
                        }
                    })
                } else {
                    dbo.collection(collection_name).find(json_queries, projection).sort(sort_opt).limit(limit_record).toArray(function (err, result) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(result);
                            console.log('found documents:  ', result.length);
                            dtb.close();
                        }
                    })
                }


            }
        });
    })
}


module.exports.update_info_schema = update_info_schema;
module.exports.insert_one_data = insert_one_data;
module.exports.insert_many_data = insert_many_data;
module.exports.query_data = query_data;