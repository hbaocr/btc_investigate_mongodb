//https://docs.aws.amazon.com/code-samples/latest/catalog/javascript-ec2-ec2_startstopinstances.js.html
let exec = require('child_process').exec;
const axios = require('axios');
const HTTP_TIME_OUT_MS = 15000;
const DELAY_START_INSTANCE_AFTER_STOP_MIN=10;
const PERIOD_CHECK_SERVICE_RUN_ON_BTC_SEC = 10; 
let max_err_cnt = 6;
let err_cnt = 0;
let cmd_start_up="./remote_start_app.sh";

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({ region: 'us-east-1' });
// Create EC2 service object
var ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });

// var params = {
//     InstanceIds: ['i-0deda85698cdab71e'],
//     DryRun: true
// };

function delay_promise(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function exec_batch_cmd(str_cmd) {
    return new Promise((resolve, reject) => {
        let dir = exec(str_cmd, function (err, stdout, stderr) {
            if (err) {
                reject(err);
            }
            else {
                resolve(stdout);
                console.log(stdout);
            }
        });
    });
}

let btc_ec2_node_id = 'i-0deda85698cdab71e';
function start_aws_instance(ec2_id) {
    return new Promise((resolve, reject) => {
        var params = {
            InstanceIds: [ec2_id],
            DryRun: true
        };
        ec2.startInstances(params, function (err, data) {
            if (err && err.code === 'DryRunOperation') {
                params.DryRun = false;
                ec2.startInstances(params, function (err, data) {
                    if (err) {
                        // console.log("Error", err);
                        reject(err);
                    } else if (data) {
                        resolve(data.StartingInstances);
                        // console.log("Success", data.StartingInstances);
                    }
                });
            } else {
                reject("You don't have permission to start instances.");
                //console.log("You don't have permission to start instances.");
            }
        });
    });

}

function stop_aws_instance(ec2_id) {
    return new Promise((resolve, reject) => {
        var params = {
            InstanceIds: [ec2_id],
            DryRun: true
        };
        // Call EC2 to stop the selected instances
        ec2.stopInstances(params, function (err, data) {
            if (err && err.code === 'DryRunOperation') {
                params.DryRun = false;
                ec2.stopInstances(params, function (err, data) {
                    if (err) {
                        //console.log("Error", err);
                        reject(err);
                    } else if (data) {
                        resolve(data.StoppingInstances);
                        //console.log("Success", data.StoppingInstances);
                    }
                });
            } else {
                reject("You don't have permission to stop instances");
                //console.log("You don't have permission to stop instances");
            }
        });
    });

}

function start_monitor_aws_instance(ec2_id) {
    return new Promise((resolve, reject) => {
        var params = {
            InstanceIds: [ec2_id],
            DryRun: true
        };
        // Call EC2 to start monitoring the selected instances
        ec2.monitorInstances(params, function (err, data) {
            if (err && err.code === 'DryRunOperation') {
                params.DryRun = false;
                ec2.monitorInstances(params, function (err, data) {
                    if (err) {
                        // console.log("Error", err);
                        reject(err);
                    } else if (data) {
                        //console.log("Success", data.InstanceMonitorings);
                        resolve(data.InstanceMonitorings);
                    }
                });
            } else {
                reject("You don't have permission to change instance monitoring.");
                // console.log("You don't have permission to change instance monitoring.");
            }
        });
    });

}

function stop_monitor_aws_instance(ec2_id) {
    return new Promise((resolve, reject) => {
        var params = {
            InstanceIds: [ec2_id],
            DryRun: true
        };
        // Call EC2 to start monitoring the selected instances
        ec2.unmonitorInstances(params, function (err, data) {
            if (err && err.code === 'DryRunOperation') {
                params.DryRun = false;
                ec2.unmonitorInstances(params, function (err, data) {
                    if (err) {
                        // console.log("Error", err);
                        reject(err);
                    } else if (data) {
                        //console.log("Success", data.InstanceMonitorings);
                        resolve(data.InstanceMonitorings);
                    }
                });
            } else {
                reject("You don't have permission to change instance monitoring.");
                // console.log("You don't have permission to change instance monitoring.");
            }
        });
    });

}

function get_status_aws_instance(ec2_id) {
    return new Promise((resolve, reject) => {
        var params = {
            InstanceIds: [ec2_id],
            DryRun: true
        };
        // Call EC2 to start monitoring the selected instances
        ec2.describeInstanceStatus(params, function (err, data) {
            if (err && err.code === 'DryRunOperation') {
                params.DryRun = false;
                ec2.describeInstanceStatus(params, function (err, data) {
                    if (err) {
                        // console.log("Error", err);
                        reject(err);
                    } else if (data) {
                        //console.log("Success", data.InstanceMonitorings);
                        resolve(data.InstanceStatuses);
                    }
                });
            } else {
                reject("You don't have permission to change instance monitoring.");
                // console.log("You don't have permission to change instance monitoring.");
            }
        });
    });

}

function is_instance_running(btc_ec2_node_id) {
    return get_status_aws_instance(btc_ec2_node_id).then(res => {
        if (res.length > 0) {
            console.log("Instance State : " + res[0].InstanceState.Name);
            if (res[0].InstanceState.Name == "running") {

                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        } else {
            return Promise.reject('Instance stop');
        }

    }).catch(e => {
        return Promise.reject(e);
    })
}

function is_instance_stop(btc_ec2_node_id) {
    return get_status_aws_instance(btc_ec2_node_id).then(res => {
        if (res.length > 0) {
            return Promise.resolve(false);

        } else { /**Stop ==> No state found */
            return Promise.resolve(true);
        }

    }).catch(e => {
        reject(e);
    })
}


async function do_start_aws_instance(btc_ec2_node_id) {
    let res;
    try {
        res = await is_instance_stop(btc_ec2_node_id);
        if (res) {
            res = await start_aws_instance(btc_ec2_node_id);
            console.log(res);
            while (1) {
                try {
                    await delay_promise(10000);
                    res = is_instance_running(btc_ec2_node_id);
                    if (res) {
                        break;
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            while (1) {
                try {
                    await delay_promise(3*60000);//delay  3min for sure start
                    await exec_batch_cmd(cmd_start_up);
                    break;
                } catch (error) {
                    console.error(error);
                    let e = '' + error;
                    if (e.indexOf('Connection refused') >= 0) {

                    } else {
                        break;
                    }
                }
            }

        } else {
            console.log("Instance are running");
        }
    } catch (error) {
        console.error(error);
    }
}

async function do_stop_aws_instance(btc_ec2_node_id) {
    let res;
    while (1) {
        try {
            res = await stop_aws_instance(btc_ec2_node_id);
            while (1) {
                try {
                    await delay_promise(10000);
                    res = await is_instance_stop(btc_ec2_node_id);
                    if (res) {
                        console.log('Instance is stop');
                        break;
                    }

                } catch (error) {
                   // process.exit(1);
                }
            }
            break;
        } catch (error) {
            //process.exit(1);
        }
    }
}

async function is_btc_node_working() {
    return axios.get("http://35.170.128.185:9615",{
        timeout: HTTP_TIME_OUT_MS
      }).then(res => {
        if (res) {
            console.log(res.statusText + ' :' + res.status);
            if (res.status == 200) {
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        }
    }).catch(e => {
        return Promise.reject(e);
    })
}


async function main_app() {
    // do_stop_aws_instance(btc_ec2_node_id);
    let res;
    res =await get_status_aws_instance(btc_ec2_node_id);
    while (1) {
        //loop to check status until error
        while (1) {

            try {
                await delay_promise(PERIOD_CHECK_SERVICE_RUN_ON_BTC_SEC*1000);
                res = await is_btc_node_working();
                err_cnt = 0;
                console.log('btc_node ok');
            } catch (error) {
                console.log('btc_node error at cnt ' + err_cnt + '/' + max_err_cnt);
                err_cnt++;
                if (err_cnt > max_err_cnt) {
                    err_cnt=0;
                    console.log('btc_node force to restart ');
                    break;
                }
            }
        }
        //=========================Stop then restart=================
        try {
            console.log('>>>>>>>>>>>>>>>>>>Stop EC2>>>>>>>>>>>>>>>>>>>>>')
            await do_stop_aws_instance(btc_ec2_node_id);
        } catch (error) {
            console.log('Stop EC2 err');
        }
        for( let i=DELAY_START_INSTANCE_AFTER_STOP_MIN;i >0;i--){
            console.log('remain '+i +' min');
            await delay_promise(60000);
        }
        try {
            console.log('>>>>>>>>>>>>>>>>>Start EC2 again>>>>>>>>>>>>>>>>>>>>>>')
            await do_start_aws_instance(btc_ec2_node_id);
        } catch (error) {
            console.log('Start EC2 err');
        }
        
       // delay 5 min to start check again
       for( let i=5 ;i >0;i--){
        console.log(' new loop checked loop  will start in '+i +' min');
        await delay_promise(60000);
        }
    }
}

main_app();