let sys = require('util')
let exec = require('child_process').exec;

let portscanner = require('portscanner');
const period_check = 5000;//10s to schk
const reset_count = 3;
let port_cnt = [];
function delay_promise(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function build_cmd_start_shard_server(shardnum, port) {
    let replSetID = 'rs' + shardnum;
    let bindIp = '0.0.0.0';
    let dbpathName = `/home/ubuntu/shard${shardnum}/data`;
    let logFile = `/home/ubuntu/shard${shardnum}/log.txt`
    let cmd = `mongod  --shardsvr --replSet ${replSetID}  --bind_ip ${bindIp}  --port ${port}  --dbpath ${dbpathName} --fork --logpath ${logFile} --logRotate reopen --logappend`
    return cmd;
}
function build_cmd_start_configShard_server(port) {
    let replSetID = "csrs0";
    let bindIp = '0.0.0.0';
    let dbpathName = `/home/ubuntu/mongoServConfig/data`;
    let logFile = `/home/ubuntu/mongoServConfig/log.txt`
    let cmd = `mongod  --shardsvr --replSet ${replSetID}  --bind_ip ${bindIp}  --port ${port}  --dbpath ${dbpathName} --fork --logpath ${logFile} --logRotate reopen --logappend`
    return cmd;
}

function build_cmd_start_mongos_router(port = 27017) {
    let logFile = "/dev/null";

    let cfgServerReplica = "csrs0";
    let serverCfgPort = 27018;
    let serverCfgIP = "127.0.0.1";
    //mogos router interface
    let bindIP = "0.0.0.0";
    let routerPort = port;
    let cmd = `mongos --configdb ${cfgServerReplica}/${serverCfgIP}:${serverCfgPort} --port ${routerPort} --bind_ip ${bindIP} --fork --logpath ${logFile} --logRotate reopen --logappend`;
    return cmd;
}


function port_scanner_async(port, ip) {
    return new Promise((resolve, reject) => {
        portscanner.checkPortStatus(port, ip, function (error, status) {
            if (error) {
                reject(error);
            } else {
                resolve(status);
            }
        })
    });

}
function exec_batch_cmd(str_cmd) {
    return new Promise((resolve, reject) => {
        let dir = exec(str_cmd, function (err, stdout, stderr) {
            if (err) {
                reject(err);
            }
            else {
                resolve(stdout);
                console.log(stdout);
            }
        });

        // dir.on('exit', function (code) {
        //     // exit code is code
        // });
    });


}
async function scan_the_mongodb_port_task() {
    let ip = '127.0.0.1';


    // Check status of config server
    let port = 27018;//StartconfigServer 27018
    await port_scanner_async(port, ip).then(async function (status) {

        let key = 'p' + port;
        if (status == 'closed') {
            if (port_cnt[key] == undefined) {
                port_cnt[key] = 0;
            } else {
                port_cnt[key] += 1;
            }
        } else {
            port_cnt[key] = 0;
        }
        // Status is 'open' if currently in use or 'closed' if available
        console.log(`port ${port} is ${status} at count ${port_cnt[key]} per ${reset_count}`);
        if (port_cnt[key] > reset_count) {
            port_cnt[key] = 0;
            let cmd = build_cmd_start_configShard_server(port);
            console.log(cmd);
            await exec_batch_cmd(cmd);
        }
    })

    for (let shard = 0; shard < 7; shard++) {
        // Check status of shard server
        port = 27019 + shard//StartconfigServer 27018
        await port_scanner_async(port, ip).then(async function (status) {
            let key = 'p' + port;
            if (status == 'closed') {
                if (port_cnt[key] == undefined) {
                    port_cnt[key] = 0;
                } else {
                    port_cnt[key] += 1;
                }
            } else {
                port_cnt[key] = 0;
            }
            // Status is 'open' if currently in use or 'closed' if available
            console.log(`port ${port} is ${status} at count ${port_cnt[key]} per ${reset_count}`);
            if (port_cnt[key] > reset_count) {
                port_cnt[key] = 0;
                let cmd = build_cmd_start_shard_server(shard, port);
                console.log(cmd);
                await exec_batch_cmd(cmd);
            }
        })
    }

    // Checks the status of a single port
    // Check status of MongosRouter server
    port = 27017; // MongosRouter
    await port_scanner_async(port, ip).then(async function (status) {

        let key = 'p' + port;
        if (status == 'closed') {
            if (port_cnt[key] == undefined) {
                port_cnt[key] = 0;
            } else {
                port_cnt[key] += 1;
            }
        } else {
            port_cnt[key] = 0;
        }
        // Status is 'open' if currently in use or 'closed' if available
        console.log(`port ${port} is ${status} at count ${port_cnt[key]} per ${reset_count}`);
        if (port_cnt[key] > reset_count) {
            port_cnt[key] = 0;
            let cmd = build_cmd_start_mongos_router(port);
            console.log(cmd);
            await exec_batch_cmd(cmd);
        }
    })
}

async function main_async() {
    while (1 == 1) {
        await delay_promise(period_check);
        try {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>');
            await scan_the_mongodb_port_task();
        } catch (error) {
            console.error(error);
        }
    }
}
main_async();