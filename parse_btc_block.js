
let sha1 = require('sha1');

//batch_res.data
function parse_rawtx_to_tx_schema(resp_data) {
    let tx_info_schema = {};
    tx_info_schema._id = resp_data.result.txid;
    tx_info_schema.block_height = resp_data.result.height;
    tx_info_schema.block_time = resp_data.result.blocktime;
    // 
    tx_info_schema.light_derived_froms = resp_data.result.vin.map((vin) => {
        if (vin.coinbase == undefined) { // normal Txid
            return {
                txid: vin.txid,
                vout: vin.vout /* get from txid at vout order */
            }
        } else {
            return {
                txid: vin.coinbase,
                vout: -1 /*coinbase Transaction */
            }
        }
    });

    // only count the standard output with address. The output without address are not counted 
    let tmp = resp_data.result.vout.map((v) => {
        let def_ret = {
            addresses: [],
            amount: v.value,
            idx: v.n
        }
        switch (v.scriptPubKey.type) {
            case 'multisig': // return the array of adrrs in mutisig
            case 'scripthash'://p2pSH --> return array with only one addrs
            case 'pubkeyhash': //p2pKH --> return array with only one addrs
            case 'pubkey': //p2pK --> return array with only one addrs
            case 'witness_v0_keyhash':
                def_ret = {
                    tx_type:v.scriptPubKey.type,
                    addresses: v.scriptPubKey.addresses,// array of output
                    amount: v.value,
                    idx: v.n
                }
                break;
            case 'nulldata'://OP ret
            
                break;
            default:
               
        }
        return def_ret;
    })
    tx_info_schema.to_addresses = tmp.filter((v)=>{
        try{
            if(v.addresses.length==0){
                return false; // filter out this one
            }else{
                return true;
            }
        }catch(e){
            console.log(e);
        }
    });
    return tx_info_schema;
}

/**
 * 
 * @param {*} resp_data : the resp_data element of batch_resp
 * @param {*} array_addr_out : this variable will absort parsed addr data 
 */
function parse_rawtx_to_addr_schema(resp_data,array_addr_out){
    let _txid = resp_data.result.txid;
    let _block_height = resp_data.result.height;
    let _block_time = resp_data.result.blocktime;
    let _is_dark = 0;
    // only count the standard output with address. The output without address are not counted 
    let tmp = resp_data.result.vout.map((v) => {
        let def_ret = {
            addresses: [],
            amount: v.value,
            idx: v.n
        }
        switch (v.scriptPubKey.type) {
            case 'multisig': // return the array of adrrs in mutisig
            case 'scripthash'://p2pSH --> return array with only one addrs
            case 'pubkeyhash': //p2pKH --> return array with only one addrs
            case 'pubkey': //p2pK --> return array with only one addrs
                def_ret = {
                    tx_type:v.scriptPubKey.type,
                    addresses: v.scriptPubKey.addresses,// array of output
                    amount: v.value,
                    idx: v.n
                }
                break;
            case 'nulldata'://OP ret
                break;
            default:
               
        }
        return def_ret;
    })
    let valid_to_addresses = tmp.filter((v)=>{
        try{
            /* only accept p2pkh which have only one adrress at output. Length=1
                the op_ret have no addr.Length =0
                the multisig for exampe 2_3 multisig will have 3 address at output. Length =3
            */
            if(v.addresses.length != 1){ 
                return false; // filter out this one
            }else{
                return true;
            }
        }catch(e){
            console.log(e);
        }
    });
    valid_to_addresses.forEach(element => {
        /**
         * note that, only accept p2pkh which have 1 output. the multisig not process here
         * */ 
        let addr = element.addresses[0];
        let to_value = element.amount;
        let addr_inf ={
            txid:_txid,
            blocktime:_block_time,
            blockheight:_block_height,
            is_dark:_is_dark,
            amount:to_value
        };
        if(array_addr_out[addr]==undefined){ /* new address */
            array_addr_out[addr]=[];
            array_addr_out[addr].push(addr_inf);
        }else{
            array_addr_out[addr].push(addr_inf);
        }
    });
    return array_addr_out;
}


// let addr_doc = {
//     in_tx: from_txid,// the txid tranfer money to this addr.
//     in_tx_idx:vout.n,// the index of this addr in output of this in_tx ( idx of addr in vout)
//     addr: vout.scriptPubKey.addresses,// array of output (if multisig array >1 element, if normal, there is only one addr in this)
//     addr_type:  'scripthash',// the type of locking script
//     wallet_type:'personal',//or miner 
//     amount: vout.value,// the value to addr
//     is_spent: 0, /** 0 : UTXO , 1 --> spent */
//     /** when this addr info is spent --> these fields are active */
//     out_tx: '', // the txid spends this addr. The txid use  (in_tx,in_tx_idx) as the vin to spend
//     out_at_vin_idx: -1, /* the idx of set (in_tx,in_tx_idx) in vin array of out_tx*/
// };

function parse_vout_info(txid_in, vout) {
    let addr_doc = {
        in_tx: txid_in,
        in_tx_idx:'',
        amount:0,/** default */
        is_spent: 0, /** 0 : UTXO , 1 --> spent */
        /** when this addr info is spent --> these fields are active */
        out_tx: '', // this field is updated when spent ( spent txid). The txid use this in_tx as the input to spend
        out_at_vin_idx: -1, /* the idx of target addr in out_tx(spent Txid)*/
    };
    addr_doc.wallet_type = 'personal';//default
    addr_doc.addr_type = vout.scriptPubKey.type;
    addr_doc.in_tx_idx = vout.n;// the index of this addr in output of this in_tx ( idx of addr in vout)
    addr_doc.amount = vout.value;// the value to addr
    switch (vout.scriptPubKey.type) {

        case 'multisig': // return the array of adrrs in mutisig
        case 'scripthash'://p2pSH --> return array with only one addrs
        case 'pubkeyhash': //p2pKH --> return array with only one addrs
        case 'pubkey': //p2pK --> return array with only one addrs
        case 'witness_v0_keyhash':// for native segwit(Bench32) addrress
            addr_doc.addr = vout.scriptPubKey.addresses;// array of output
            break;
        case 'nulldata'://OP ret
            addr_doc.addr= [];
            break;
        default:
            if(vout.scriptPubKey.addresses==undefined){
                addr_doc.addr= [];
            }else{
                addr_doc.addr = vout.scriptPubKey.addresses;// array of output
            }
    }
    return addr_doc;
}

function parse_vin_info(spending_txid, pos_of_txid_in, vin) {
    function calc_addr_info_id(txid,idx) {
        let str = txid + idx;
        return sha1(str);
    }
    let info = {
        is_spent: 1, /** 0 : UTXO , 1 --> spent */
        /** when this addr info spent --> these field are active */
        out_tx: spending_txid,
        out_at_vin_idx: pos_of_txid_in,/* pos of income_txid in vin OF SPENT TXID*/
    };
    let id=calc_addr_info_id(vin.txid,vin.vout);
    let filter={_id:id};
    //let filter = { in_tx: vin.txid, in_tx_idx: vin.vout }

    return { filter: filter, info: info };
}


module.exports.parse_rawtx_to_tx_schema =parse_rawtx_to_tx_schema;
module.exports.parse_rawtx_to_addr_schema=parse_rawtx_to_addr_schema;
module.exports.parse_vout_info=parse_vout_info;
module.exports.parse_vin_info=parse_vin_info;