/**
 * TX_info collection Schema :
 * 
 * _id = txid
 * // these 2 fields are updated through background task check
 * dark_height = min {dark_derived_from.txid.dark_height}?min {dark_derived_from.txid.dark_height}+1 : 0
 * dark_derived_from =[{txid ,vout},{txid,vout}, .... ] 
 * 
 * // these are updated from bitcoind node. vout =-1 (coinbase txid)
 * light_derived_froms =[{txid ,vout},{txid,vout}, ....] // full from Txid
 * to_addresses   = [{addresses: [ad1, ad2,...],tx_type,amout_delta} , ...]
 * date =
 * block=
 */

 /****
  * Address_info collection schema
  * 
  * _id : address
  * txid_to_this =[{txid, block_num,time,amount_sat} , .... ]
  * 
  */

const sha1 = require('sha1');
const sys_config = require('./config/config');
const parse_block_utils = require('./parse_btc_block');

function build_tx_schema_mongodb_ops(rpc_batch_resp){
    let tx_data = rpc_batch_resp.data.map((element, idx) => {
        element.result.idx = idx;
        element.result.height = rpc_batch_resp.height;
        return parse_block_utils.parse_rawtx_to_tx_schema(element);
    })
    return tx_data;
}

// let filter={ _id: "0xadd...." };
// add to array of data, if the elements are already exist, it will be bypassed
// let json_ops={ $addToSet: { txid_to_this: { $each: [ {txid:'1233',amount:10,time:100},{txid:'1232',amount:10,time:100}] } } }
// await mongodb_client.update_info_schema(dtb_name,block_collection,filter,json_ops);
function build_addr_schema_mongodb_ops(rpc_batch_resp){
    let addr_info=[];
    rpc_batch_resp.data.forEach((element, idx) => {
        element.result.idx = idx;
        element.result.height = rpc_batch_resp.height;
        parse_block_utils.parse_rawtx_to_addr_schema(element,addr_info);
    });
    let json_ops=[];
    for (var key in addr_info){
        if (addr_info.hasOwnProperty(key)){
            //console.log(key);
            let op={};
            //op.filter={_id:key}; --> dont't fix _id to use in bucketting mode.let _id ( collection id)
            //control automatically by mongodb when it reach the end of limited
            let n_max_element = sys_config.mongodb.addr_info_collection.n_max_element;
            op.filter={address:key,count:{ $lt : n_max_element}};
            op.update_query={
                $addToSet: { txid_to_this: { $each:addr_info[key]}},
                $inc: { count: 1 }
            };
            json_ops.push(op);
        }
    }
    return json_ops;
}


// let addr_doc = {
//     in_tx: from_txid,// the txid tranfer money to this addr.
//     in_tx_idx:vout.n,// the index of this addr in output of this in_tx ( idx of addr in vout)
//     addr: vout.scriptPubKey.addresses,// array of output (if multisig array >1 element, if normal, there is only one addr in this)
//     addr_type:  'scripthash',// the type of locking script
//     wallet_type:'personal',//or miner 
//     amount: vout.value,// the value to addr
//     is_spent: 0, /** 0 : UTXO , 1 --> spent */
//     /** when this addr info is spent --> these fields are active */
//     out_tx: '', // the txid spends this addr. The txid use  (in_tx,in_tx_idx) as the vin to spend
//     out_at_vin_idx: -1, /* the idx of set (in_tx,in_tx_idx) in vin array of out_tx*/
// };
function build_addr_infos(rpc_batch_resp) {
    function calc_addr_info_id(addr_doc) {
        let str = addr_doc.in_tx + addr_doc.in_tx_idx;
        return sha1(str);
    }
    let insert_income_addr_info = [];//insert income
    let update_spent_addr_info = [];//update spent

    rpc_batch_resp.data.forEach((tx_info, tx_idx) => {
        let txid = tx_info.result.txid;
        let vout_array = tx_info.result.vout;// where target coin to --> use for insert new addr info 
        let vin_array = tx_info.result.vin; // where spent previous--> use for update
        let height =tx_info.result.height;
        let time =tx_info.result.blocktime;
        if (tx_idx == 0) {
            /* this is coinbase tx. Vin[0] from coinbase, Vout[0] is coinbase addr */
            vout_array.forEach((v, v_idx) => {
                let info = parse_block_utils.parse_vout_info(txid, v);
                info._id = calc_addr_info_id(info);
                info.wallet_type = 'miner';
                info.height=height;
                info.time=time;
                info.spent_at_height=-1;
                insert_income_addr_info.push(info);
            })
            // because vin is coinbase --> ignore
            // vin_array.forEach((v,v_idx)=>{
            //     let info=parse_vin_info(txid,v_idx,v);
            //     update_spent_addr_info.push(info);
            // })
        } else {
            vout_array.forEach((v, v_idx) => {
                let info = parse_block_utils.parse_vout_info(txid, v);
                info._id = calc_addr_info_id(info);
                info.height=height;
                info.time=time;
                info.spent_at_height=-1;
                insert_income_addr_info.push(info);
            })

            vin_array.forEach((v, v_idx) => {
                let info = parse_block_utils.parse_vin_info(txid, v_idx, v);
                info.info.spent_at_height=height;
                update_spent_addr_info.push(info);
            })
        }

    });
    return { insert: insert_income_addr_info, update: update_spent_addr_info };
}


//change to synchronus way to avoid the error "MongoNetworkError: write EPIPE"
function chunk_the_array(array_in, chunk_sz) {
    let chunks=[]
    let n_element = array_in.length;
    let n_chunk = Math.ceil(n_element / chunk_sz);
    for (let i = 0; i < n_chunk;i++) {
        let end_p = chunk_sz * (i + 1);
        if (end_p > n_element) {
            end_p = n_element;
        }
        let chunk = array_in.slice(chunk_sz * i, end_p);
        if(chunk.length>0){
            chunks.push(chunk);
        }
    }
    return chunks;
}

module.exports.build_tx_schema_mongodb_ops =build_tx_schema_mongodb_ops;
module.exports.build_addr_schema_mongodb_ops=build_addr_schema_mongodb_ops;
module.exports.build_addr_infos=build_addr_infos;
module.exports.chunk_the_array=chunk_the_array;