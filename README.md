## Get data from bitcoind and save to mongodb

* The config of system in `config/config.js` before starting, modify this file to let this app match with your config
* Start mongodb
    * The version of mongodb in this system is `v4.0.4`. Check it by run `mongod --version`. If it is failure, you need to install mongodb in your system.
    * Run this cmd to start mongodb `./start_mongodb`
* Run 
    * `npm install`
    * `node index.js` to start
* The reference link
    * [Mongodb queries the array in documents](https://docs.mongodb.com/manual/tutorial/query-array-of-documents/)
    * [From SQL to mongodb queries](https://docs.mongodb.com/manual/reference/sql-comparison/)
    * [Data model](https://docs.mongodb.com/manual/applications/data-models/)
    * [Graph](https://www.compose.com/articles/graph-data-with-mongodb/)
    * [Mongodb graph lookup](https://docs.mongodb.com/manual/reference/operator/aggregation/graphLookup/)
    * [webinar-working-with-graph-data-in-mongodb](https://www.slideshare.net/mongodb/webinar-working-with-graph-data-in-mongodb)


* Error Note need to reupdate when finish sync

* Before block 291221 only update addr_info with limited 5000 ( bug incode); need to recheck if there are any block have addr_info greater than 5000 need to reupdate.for (block= 291221,block >1,block--){if addr_info.insert.length >5000 --> update}