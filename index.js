/**
 * TX_info collection Schema :
 * 
 * _id = txid
 * // these 2 fields are updated through background task check
 * dark_height = min {dark_derived_from.txid.dark_height}?min {dark_derived_from.txid.dark_height}+1 : 0
 * dark_derived_from =[{txid ,vout},{txid,vout}, .... ] 
 * 
 * // these are updated from bitcoind node. vout =-1 (coinbase txid)
 * light_derived_froms =[{txid ,vout},{txid,vout}, ....] // full from Txid
 * to_addresses   = [{addresses: [ad1, ad2,...],tx_type,amout_delta} , ...]
 * date =
 * block=
 */

/****
 * Address_info collection schema
 * 
 * _id : address
 * txid_to_this =[{txid, block_num,time,amount_sat} , .... ]
 * 
 */

const START_FROM_FILE_CHECK_POINT= true;
const check_point_file='./checkpoint.txt';

const fs = require('fs');
const sha1 = require('sha1');
const sys_config = require('./config/config');
const mongodb_client = require('./driver/mongodb_client');
const rpc_btc = require('./driver/btc_rpc');
//const parse_block_utils = require('./parse_btc_block');
const mongodb_btc = require('./mongodb_btc');

const dtb_name = sys_config.mongodb.dtb_name;
let rpc_host = `http://${sys_config.bitcoind.rpc_host}:${sys_config.bitcoind.rpc_port}`;
const block_collection = sys_config.mongodb.block_info_collection.name;
const txid_collection = sys_config.mongodb.tx_info_collection.name;
const adrr_collection = sys_config.mongodb.addr_info_collection.name;

const Logger = require('le_node');
let log = new Logger({
    token: '7ee032d5-f6c3-4b3c-a60d-77ff1c65527e'
});

function log_to_server(info) {
    try {
        log.log("debug", { Labels: info });
    }
    catch (e) {

    }
}


//console.log(t);
function is_critical_err(e){
    let str = ''+e;
    if(str.indexOf("write EPIPE") >= 0) return true;
    if(str.indexOf("MongoNetworkError") >= 0) return true;
    if(str.indexOf("MongoError") >= 0) return true;
    return false;
}


function get_block_from_check_point(){
    let blk_num=-1;
    if(START_FROM_FILE_CHECK_POINT==false){
        return -1;
    }
    try {
        blk_num=fs.readFileSync(check_point_file,"utf8");
    } catch (error) {
        return -1;
    }  
    return parseInt(blk_num,10);
}
function write_block_to_check_point(block_num){
    if(START_FROM_FILE_CHECK_POINT==false){
        return -1;
    }
    try {
        fs.writeFileSync(check_point_file,block_num,'utf8');
        return 1;
    } catch (error) {
        return -1;
    }
}



async function main_app() {

    let chunk_size = 5000;
    let  = 1;
    let start=[];
    start_block_height=1;
    //=====================get last saved block_info from database==================================
    try{
        start= await mongodb_client.query_data(dtb_name, block_collection, {}, {}, 1, { _id: -1 });
    } catch (error) {
        process.exit(1);
    } 

    if (start.length == 0)//no dtb save before
    {
        //message:"The genesis block 0 : coinbase is not considered an ordinary transaction and cannot be retrieved".
        console.log('restart at block ', start_block_height);
    } else {
        start_block_height = start[0].height; //update curent
        console.log('restart at block ', start_block_height);
    }

     //start_block_height = 343154;//579980;
        let check_point_block = get_block_from_check_point();
        if(check_point_block>0){
            //first recover at block 488000 to recheck the overflow HDD on shard0
            console.log('================>>>>>>>>>>>>start_with check point at -----> ',check_point_block);
            start_block_height=check_point_block;
        }
     
    // rpc_host="http://127.0.0.1:18332";//change the link to local node to test

    let cmd = rpc_btc.build_cmd_getblockhash(start_block_height);

    let res;
    try {
        res = await rpc_btc.do_http_rpc(rpc_host, cmd);
    } catch (e) {
        log_to_server('app start failed ' + e);
        console.log('app start failed ' + e);
        //await rpc_btc.delay_promise(5000);
        process.exit(1);
    }

    await rpc_btc.delay_promise(sys_config.bitcoind.req_period);
    let block_hash = res.data.result;
    let nextblockhash = undefined;
    let start_rpc_time = 0;
    do {

        if ((Date.now() - start_rpc_time) < sys_config.bitcoind.req_period) {
            await rpc_btc.delay_promise(sys_config.bitcoind.req_period);
        }
        start_rpc_time = Date.now();
        let rpc_req = [];
        cmd = rpc_btc.build_cmd_getblockcount();
        rpc_req.push(cmd);//cmd0 get lastblock
        cmd = rpc_btc.build_cmd_getblock(block_hash);
        rpc_req.push(cmd);//cmd1 getblock
        try {
            res = await rpc_btc.do_http_rpc(rpc_host, rpc_req);
        } catch (e) {
            log_to_server('bitcoind rpc err: ' + e);
            await rpc_btc.delay_promise(1000);
            //continue;
            process.exit(1);
        }

        let lasted_height = res.data[0].result;//result of cmd0
        let syncing_height = res.data[1].result.height;//result of cmd1
        nextblockhash = res.data[1].result.nextblockhash;

        if (nextblockhash == undefined) {
            console.log('reach the current block.Wait for new block comming.....');
            log_to_server("lasted blocked synced");
            await rpc_btc.delay_promise(10000);//10 sec to check if new block is comming
            continue;
        }

        if (lasted_height == undefined) continue;
        if ((syncing_height + sys_config.bitcoind.reserved_mature_block) > lasted_height) {
            console.log('reach the matured block.Wait for new block comming.....' + syncing_height + '/' + lasted_height);
            log_to_server("mature blocked synced. Threshold = " + sys_config.bitcoind.reserved_mature_block);
            await rpc_btc.delay_promise(10000);//10 sec to check if new block is comming
            continue;
        }

        //=====================Save block info into database=============================================== 
        console.log('>>>>>>>>>>>>>>>> block heigh = ' + syncing_height + '   blockhash = ' + block_hash);
        let json_block = rpc_btc.block_obj_to_json(res.data[1].result);
        try {
            let r = await mongodb_client.insert_one_data(dtb_name, block_collection, json_block);
            console.log('>>save block_info to mongodb ');
        } catch (e) {
            if (e.code == 11000) {
                console.log('********duplicate block data( already saved)');
            } else {
                console.log('---->try again');
                console.log('---->' + e);
                log_to_server('mongodb save block err : ' + e);
                if(is_critical_err(e)){
                    process.exit(1);
                }
                continue;
            }
        }

        //=======================Get all data in block from the full node============================================== 
        let json_batch = res.data[1].result.tx.map((e) => {
            return rpc_btc.build_cmd_getrawtransaction(e);
        })
        let height = res.data[1].result.height;
        if (height % 20 == 0) {
            log_to_server('block saving at height: ' + height);
        }

        let rpc_batch_res;
        await rpc_btc.delay_promise(sys_config.bitcoind.req_period);
        //while (1) {
        try {
            rpc_batch_res = await rpc_btc.do_http_rpc(rpc_host, json_batch);
            rpc_batch_res.height = height; //add more blockheight field
            //break;
        } catch (e) {
            console.log('batch request rpc failed --> try again');
            log_to_server('bitcoind rpc getblock data err: ' + e);
            process.exit(1);
           // await rpc_btc.delay_promise(1000);//1 sec to retry
           // continue;
        }
        //}

        //========================Save tx_info into database=============================================== 
        let tx_info=[];
        try {
            tx_info = mongodb_btc.build_tx_schema_mongodb_ops(rpc_batch_res);
        } catch (e) {
            console.log('batch request rpc failed --> try again');
            log_to_server('bitcoind rpc tx_info  data err: ' + e);
            process.exit(1);
        }
        
        if(tx_info.length==0){
            console.log('batch request rpc failed --> no tx_info. Try again');
            process.exit(1);
        }
        //to AVOID OVERLOAD for mongodb--> chunk to smaller size

        let chunks = mongodb_btc.chunk_the_array(tx_info, chunk_size);
        let tt = 0;
        let n_tx = tx_info.length;
        for (let i = 0; i < chunks.length;) {
            await rpc_btc.delay_promise(5);//50 msec
            let smaller_tx_chunk = chunks[i];
            try {
                let r = await mongodb_client.insert_many_data(dtb_name, txid_collection, smaller_tx_chunk);
                tt = tt + smaller_tx_chunk.length;
                i++;//move next
                console.log('----> In block ' + height + ', save tx_info to mongodb:  ' + (tt) + ' / ' + n_tx);
            } catch (e) {
                if (e.code == 11000) {
                    tt = tt + smaller_tx_chunk.length;
                    i++;//move next
                    console.log('----> In block ' + height + ', (duplicate)saved tx_info before to mongodb:  ' + (tt) + ' / ' + n_tx);
                } else {
                    console.log('---->' + e);
                    log_to_server('mongodb save tx_info err : ' + e);
                    await rpc_btc.delay_promise(5000);//0.5 sec to retry
                    if(is_critical_err(e)){
                        process.exit(1);
                    }
                }
            }
        }

        //================= the turn for update the addr_info=======================
        let addr_info = mongodb_btc.build_addr_infos(rpc_batch_res);
        //=========update addr_doc_income===============
        let doc_chunks = mongodb_btc.chunk_the_array(addr_info.insert, chunk_size);
        tt = 0;
        n_tx = addr_info.insert.length;

        for (let i = 0; i < doc_chunks.length;) {
            await rpc_btc.delay_promise(1);//50 msec
            let chunk = doc_chunks[i];
            try {
                let r = await mongodb_client.insert_many_data(dtb_name, adrr_collection, chunk);
                tt = tt + chunk.length;
                i++;//move next
                console.log('----> In block ' + height + ', save address_info to mongodb:  ' + (tt) + ' / ' + n_tx);
            } catch (e) {
                if (e.code == 11000) {
                    tt = tt + chunk.length;
                    i++;//move next
                    console.log('*******In block ' + height + ', (duplicate)saved addr_info before to mongodb:  ' + (tt) + ' / ' + n_tx);
                } else {
                    console.log('---->' + e);
                    log_to_server('mongodb addr_info err: ' + e);
                    await rpc_btc.delay_promise(5000);//0.5 sec to retry
                    if(is_critical_err(e)){
                        process.exit(1);
                    }
                }
            }
        }

        //=========update addr_doc_spent===============
        n_tx = addr_info.update.length;
        let CHUNK_NUM =5000;
        console.log('update spent Txs: ' +n_tx);
        for (i = 0; i < n_tx;) {
            let filter = addr_info.update[i].filter;
            let info_ops = { $set: addr_info.update[i].info };
            try {
                await rpc_btc.delay_promise(1);
                let r = await mongodb_client.update_info_schema(dtb_name, adrr_collection, filter, info_ops, { upsert: false });
                i++;
                if(i%CHUNK_NUM==(CHUNK_NUM-2)){
                    console.log('........');
                    await rpc_btc.delay_promise(5000);
                }
                //console.log('----> In block ' + height + ', update spent info to mongodb:  ' + (i) + ' / ' + n_tx);
            } catch (e) {
                console.log('---->' + e);
                log_to_server('mongodb update_spent err: ' + e);
                //await rpc_btc.delay_promise(5000);//0.5 sec to retry
                if(is_critical_err(e)){
                    process.exit(1);
                }
            }
        }
        //================= the turn for update the addr_info=======================
        block_hash = nextblockhash;
        write_block_to_check_point(height);
    } while (1 == 1)
}

main_app();
