const sys_config = require('./config/config');
const mongodb_client = require('./driver/mongodb_client');
const rpc_btc = require('./driver/btc_rpc');
//const parse_block_utils = require('./parse_btc_block');
const mongodb_btc = require('./mongodb_btc');

const dtb_name = 'test';
const rpc_host = `http://${sys_config.bitcoind.rpc_host}:${sys_config.bitcoind.rpc_port}`;

const adrr_collection = 'test_data_collect';


async function main_app() {

    // db.sensorReadings.update(
    //     { name: "SensorName1", count { $lt : 500} },
    //     {
    //         //Your update. $push your reading and $inc your count
    //         $push: { readings: [ReadingDocumentToPush] }, 
    //         $inc: { count: 1 }
    //     },
    //     { upsert: true }
    // )

    //create the string of 10K a
    let str_test_10k_data=""
    for(let i=0;i<10000;i++){
        str_test_10k_data= str_test_10k_data+"a";
    }

    //30M data  insert to collection
    let n_test= 30;
    let m_max_update =10;
    for(i=0;i<n_test;){
        try{
            let dat =i+"__"+str_test_10k_data;
            ReadingDocumentToPush={info:dat,inc:i };
            let filter= { name: "SensorName1", count:{ $lt : m_max_update} };
            let update_data={
                $addToSet: { txid_to_this: { $each:[ReadingDocumentToPush]}},
                $inc: { count: 1 }
            };
            let r = await mongodb_client.update_info_schema(dtb_name, adrr_collection, filter, update_data);
            console.log('In block ' + i + ' ,save addr_info to mongodb: ' + (i + 1) + ' / ' + n_test);
            i++;//move next
        }catch(e){
            if(e.code==11000){
                console.log('Error In block ' + i + ' ,save addr_info to mongodb: ' + (i + 1) + ' / ' + n_test);
                i++;//move next
            }else{
                console.log(e);
            }
        }

    }


}
main_app();