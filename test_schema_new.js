const sha1 = require('sha1');
const sys_config = require('./config/config');
const mongodb_client = require('./driver/mongodb_client');
const rpc_btc = require('./driver/btc_rpc');
//const parse_block_utils = require('./parse_btc_block');
const mongodb_btc = require('./mongodb_btc');

const dtb_name = 'test_sche';
const rpc_host = `http://${sys_config.bitcoind.rpc_host}:${sys_config.bitcoind.rpc_port}`;

const adrr_collection = 'test_data_collect';
// let addr_info_schema = {
//     income_txid: 'txid_in',
//     addr: 'test_addr',
//     addr_idx: 0,/** pos of addr in vouts of income_txid*/
//     amount: '1',
//     is_spent: 0, /** 0 : UTXO , 1 --> spent */
//     /** when this addr info spent --> these field are active */
//     spent_by_txid: 'txid',
//     spent_by_idx: '1', /*   pos of income_txid in spent vin */
// };

function build_addr_info_doc(income_txid, addr, addr_idx, amount, is_spent = 0, spent_by_txid = '', spent_at_idx = 0) {
    let addr_doc = {
        addr: [addr, 200 + addr, 300 + addr],
        addr_type: 'unknown',
        wallet_type: 'personal',/** miner  */
        in_tx: income_txid,
        in_tx_idx: addr_idx,/**  pos of income_txid in spent vin */
        amount: amount + '',
        is_spent: is_spent, /** 0 : UTXO , 1 --> spent */
        /** when this addr info spent --> these field are active */
        out_tx: spent_by_txid,
        out_tx_idx: spent_at_idx, /*   pos this addr in spent_txid */
    };
    let str = addr_doc.in_tx + addr_doc.addr + addr_doc.in_tx_idx;
    addr_doc._id = sha1(str);
    return addr_doc;
}


function calc_addr_info_id(addr_doc) {
    let str = addr_doc.income_txid + addr_doc.addr + addr_doc.addr_idx;
    return sha1(str);
}

function parse_vout_info(from_txid, vout) {
    let addr_doc = {
        in_tx: from_txid,
        is_spent: 0, /** 0 : UTXO , 1 --> spent */
        /** when this addr info spent --> these field are active */
        out_tx: '', // this field is updated when spent ( spent txid)
        out_at_tx_idx: -1, /* pos of income_txid in vin OF SPENT TXID*/
    };
    addr_doc.addr_type = vout.scriptPubKey.type;
    switch (vout.scriptPubKey.type) {

        case 'multisig': // return the array of adrrs in mutisig
        case 'scripthash'://p2pSH --> return array with only one addrs
        case 'pubkeyhash': //p2pKH --> return array with only one addrs
        case 'pubkey': //p2pK --> return array with only one addrs
            addr_doc.addr = vout.scriptPubKey.addresses;// array of output
            addr_doc.in_tx_idx = vout.n;
            addr_doc.amount = vout.value;
            break;
        case 'nulldata'://OP ret
            break;
        default:
    }
    return addr_doc;
}

function parse_vin_info(spending_txid, pos_of_txid_in, vin) {
    let info=undefined;

    if (vin.coinbase == undefined) {
        info = {
            is_spent: 1, /** 0 : UTXO , 1 --> spent */
            /** when this addr info spent --> these field are active */
            out_tx: spending_txid,
            out_at_tx_idx: pos_of_txid_in,/* pos of income_txid in vin OF SPENT TXID*/
        };
        let filter = { in_tx: vin.txid, in_tx_idx: vin.vout }
        return { filter: filter, info: info };
    } else { /**coinbase Vin */
        return undefined;
    }
}

function build_addr_infos(rpc_batch_resp) {
    let insert_income_addr_info = [];//insert income
    let update_spent_addr_info = [];//update spent
    rpc_batch_resp.data.forEach((tx_info, tx_idx) => {
        let txid = tx_info.result.txid;
        let vout_array = tx_info.result.vout;// where target coin to --> use for insert new addr info 
        let vin_array = tx_info.result.vin; // where spent previous--> use for update

        if (tx_idx == 0) {
            /* this is coinbase tx. Vin[0] from coinbase, Vout[0] is coinbase addr */
            vout_array.forEach((v, v_idx) => {
                let info = parse_vout_info(txid, v);
                info.wallet_type=
                info._id = calc_addr_info_id(info);
                insert_income_addr_info.push(info);
            })

            vin_array.forEach((v, v_idx) => {
                let info = parse_vin_info(txid, v_idx, v);
                update_spent_addr_info.push(info);
            })

        } else {
            vout_array.forEach((v, v_idx) => {
                let info = parse_vout_info(txid, v);
                info._id = calc_addr_info_id(info);
                insert_income_addr_info.push(info);
            })

            vin_array.forEach((v, v_idx) => {
                let info = parse_vin_info(txid, v_idx, v);
                update_spent_addr_info.push(info);
            })
        }

    });
}

async function main_app() {

    let n_test = 16;
    let addr_doc = [];
    for (i = 0; i < n_test; i++) {
        d = build_addr_info_doc('txid_to_' + i, 'addr_' + (i % 5), 0, i, 0);
        addr_doc.push(d);
    }
    try {
        // let kk = await mongodb_client.query_data(dtb_name,'sc1',{addr:"addr_1",is_spent:1});
        //let kk = await mongodb_client.query_data(dtb_name,'sche',{addr:"addr_1"});
        let r = await mongodb_client.insert_many_data(dtb_name, 'sc', addr_doc)
        // console.log(kk);
    } catch (e) {
        if (e.code == 11000) {
            console.log("dup");
            // console.log('Error In block ' + i + ' ,save addr_info to mongodb: ' + (i + 1) + ' / ' + n_test);
            // i++;//move next
        } else {
            console.log(e);
        }
    }
}
main_app();